import React from "react";
import "./App.css";
import Homepage from "./homepage";
import "bootstrap/dist/css/bootstrap.css";

function App() {
  return <Homepage />;
}

export default App;
