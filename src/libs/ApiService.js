const axios = require("axios");
export default class ApiService {
  getRemainingAmount(partyName) {
    let domain = this.determineDomainByParty(partyName);
    return this.fetch(`${domain}/THB/token`, {
      method: "get"
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(error => {
        alert("Error");
        return error;
      });
  }

  postTransfer(partyName, amount, newHolder) {
    let domain = this.determineDomainByParty(partyName);
    return axios
      .post(
        `${domain}/THB/token/transfer?amount=${amount}&newHolder=${newHolder}`
      )
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(error => {
        alert("Error");
        return error;
      });
  }

  postCashIn(partyName, cashInAmount) {
    let domain = this.determineDomainByParty(partyName);
    return axios
      .post(`${domain}/THB/token/issue?amount=${cashInAmount}`)
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(error => {
        alert("Error");
        return error;
      });
  }

  postRedeem(partyName, amount) {
    let domain = this.determineDomainByParty(partyName);
    return axios
      .post(`${domain}/THB/token/redeem?amount=${amount}`)
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(error => {
        alert("Error");
        return error;
      });
  }

  determineDomainByParty(partyName) {
    let domain = "http://localhost:10050";
    if (partyName === "B") {
      domain = "http://localhost:10051";
    }
    return domain;
  }

  fetch(url, options) {
    // performs api calls sending the required authentication headers
    const headers = {
      Accept: "application/json",
      "Content-Type": "application/json"
      // Authorization: "Bearer " + this.getToken()
    };

    return axios({
      url,
      headers,
      ...options
    }).then(res => res.data);
  }
}
