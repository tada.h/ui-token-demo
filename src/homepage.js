import React, { Component } from "react";
import {
  Container,
  Jumbotron,
  Modal,
  InputGroup,
  FormControl,
  Row,
  Col,
  Button,
  Dropdown,
  DropdownButton
} from "react-bootstrap";
import BlockUi from "react-block-ui";
import "react-block-ui/style.css";
import ApiService from "./libs/ApiService";

class Homepage extends Component {
  constructor(props) {
    super(props);
    this.api = new ApiService();
  }

  componentWillMount() {
    this.setState({
      partyName: "A",
      blocking: false,
      amountToken: 0,
      showCashIn: false,
      showTransfer: false,
      showRedeem: false
    });
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData = async () => {
    let { partyName } = this.state;
    await this.handleBlocking(true);
    let nodeInfo = await this.api.getRemainingAmount(partyName);
    await this.setState({
      amountToken: nodeInfo.data.length > 0 ? nodeInfo.data[0].amount : 0
    });
    await this.handleBlocking(false);
  };

  handleCashInClose = () => {
    this.setState({
      showCashIn: false
    });
  };

  handleCashInShow = () => {
    this.setState({
      showCashIn: true
    });
  };

  handleTransferClose = () => {
    this.setState({
      showTransfer: false
    });
  };

  handleTransferShow = () => {
    this.setState({
      showTransfer: true
    });
  };

  handleRedeemClose = () => {
    this.setState({
      showRedeem: false
    });
  };

  handleRedeemShow = () => {
    this.setState({
      showRedeem: true
    });
  };

  doTransferToken = async () => {
    let { partyName, transferAmount, transferToParty } = this.state;
    await this.handleBlocking(true);
    this.setState({
      transferAmount: "",
      showTransfer: false
    });
    await this.api.postTransfer(partyName, transferAmount, transferToParty);
    this.fetchData();
    await this.handleBlocking(false);
  };

  doCashIn = async () => {
    let { partyName, cashInAmount } = this.state;
    await this.handleBlocking(true);
    this.setState({
      cashInAmount: "",
      showCashIn: false
    });
    let response = await this.api.postCashIn(partyName, cashInAmount);
    if (!response.data) {
      console.log("ERROR!!!!");
    }
    this.fetchData();
    await this.handleBlocking(false);
  };

  doRedeem = async () => {
    let { partName, redeemAmount } = this.state;
    await this.handleBlocking(true);
    this.setState({
      redeemAmount: "",
      showRedeem: false
    });
    await this.api.postRedeem(partName, redeemAmount);
    this.fetchData();
    await this.handleBlocking(false);
  };

  changeParty = async partName => {
    let changeParty = partName;
    await this.handleBlocking(true);
    await this.setState({
      partyName: changeParty
    });
    this.fetchData();
    await this.handleBlocking(false);
  };

  handleBlocking = async blockingStatus => {
    await this.setState({
      blocking: blockingStatus
    });
  };

  handleCashInOnchange = e => {
    let cashInAmount = e.target.value;
    this.setState({
      cashInAmount: cashInAmount
    });
  };

  handleTransferOnchange = e => {
    let transferAmount = e.target.value;
    this.setState({
      transferAmount: transferAmount
    });
  };

  handleTransferPartyOnchange = e => {
    let transferToParty = e.target.value;
    this.setState({
      transferToParty: transferToParty
    });
  };

  handleRedeemOnchange = e => {
    let redeemAmount = e.target.value;
    this.setState({
      redeemAmount: redeemAmount
    });
  };

  renderDropDownSwitchParty() {
    let { partyName } = this.state;
    return (
      <DropdownButton
        id="dropdown-basic-button"
        variant="secondary"
        title={"Node : " + partyName}
        onSelect={e => this.changeParty(e)}
      >
        <Dropdown.Item eventKey="A">Party A</Dropdown.Item>
        <Dropdown.Item eventKey="B">Party B</Dropdown.Item>
      </DropdownButton>
    );
  }

  renderActionButton() {
    return (
      <Row className="text-center">
        <Col>
          <Button variant="outline-dark" onClick={this.handleCashInShow}>
            Cash In
          </Button>
          &emsp;
          <Button variant="outline-dark" onClick={this.handleTransferShow}>
            Transfer
          </Button>
          &emsp;
          <Button variant="outline-dark" onClick={this.handleRedeemShow}>
            Redeem
          </Button>
        </Col>
      </Row>
    );
  }

  renderModal() {
    const { showCashIn, showTransfer, showRedeem } = this.state;

    return (
      <Container>
        <Modal show={showCashIn} onHide={this.handleCashInClose}>
          <Container>
            <Modal.Header closeButton>
              <Modal.Title>Amount Cash In</Modal.Title>
            </Modal.Header>
            <InputGroup>
              <InputGroup.Prepend>
                <InputGroup.Text>$</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                onChange={this.handleCashInOnchange}
                aria-label="Amount (to the nearest dollar)"
              />
            </InputGroup>
            <Modal.Footer>
              <Button variant="primary" onClick={this.doCashIn}>
                Submit
              </Button>
              <Button variant="secondary" onClick={this.handleCashInClose}>
                Close
              </Button>
            </Modal.Footer>
          </Container>
        </Modal>

        <Modal show={showTransfer} onHide={this.handleTransferClose}>
          <Container>
            <Modal.Header closeButton>
              <Modal.Title>Amount Transfer</Modal.Title>
            </Modal.Header>
            <InputGroup>
              <InputGroup.Prepend>
                <InputGroup.Text>$</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl onChange={this.handleTransferOnchange} />
            </InputGroup>
            <br />
            <InputGroup>
              <InputGroup.Prepend>
                <InputGroup.Text>Party</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                onChange={this.handleTransferPartyOnchange}
                aria-label="To party"
              />
            </InputGroup>

            <Modal.Footer>
              <Button variant="primary" onClick={this.doTransferToken}>
                Transfer
              </Button>
              <Button variant="secondary" onClick={this.handleTransferClose}>
                Close
              </Button>
            </Modal.Footer>
          </Container>
        </Modal>

        <Modal show={showRedeem} onHide={this.handleRedeemClose}>
          <Container>
            <Modal.Header closeButton>
              <Modal.Title>Amount Redeem</Modal.Title>
            </Modal.Header>
            <InputGroup>
              <InputGroup.Prepend>
                <InputGroup.Text>$</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl onChange={this.handleRedeemOnchange} />
            </InputGroup>

            <Modal.Footer>
              <Button variant="primary" onClick={this.doRedeem}>
                Redeem
              </Button>
              <Button variant="secondary" onClick={this.handleRedeemClose}>
                Close
              </Button>
            </Modal.Footer>
          </Container>
        </Modal>
      </Container>
    );
  }

  render() {
    const { partyName, amountToken } = this.state;
    return (
      <BlockUi tag="div" blocking={this.state.blocking}>
        <Container>
          <Row className="text-center">
            <Col>
              <img src="/img/logo-dva.png" alt="" />
            </Col>
          </Row>
          <br />
          {this.renderDropDownSwitchParty()}
          <br />
          <Jumbotron fluid>
            <Container className="text-center">
              <h1>Node : {partyName}</h1>
              <p>Amount : {amountToken} token</p>
            </Container>
          </Jumbotron>
          {this.renderActionButton()}
          {this.renderModal()}
        </Container>
      </BlockUi>
    );
  }
}
export default Homepage;
